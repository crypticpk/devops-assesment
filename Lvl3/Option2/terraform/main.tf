provider "aws" {
    access_key = "XXXXXXXXXXXXXXXXXXXXXXXXX"
    secret_key = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
    region = "us-east-1"
}

terraform {
  backend "s3" {
    bucket = "forassignment"
    key    = "state/terraform.tfstate"
    region = "us-east-1"
	
  }
}